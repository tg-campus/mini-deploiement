# Mini-déploiement

Ce projet est un petit exercice visant à déployer un super _backend_ codé en Rust. Il a pour objectif de montrer qu'on peut déployer très simplement une application. Il n'y a pas forcément besoin de sortir l'artillerie lourde. Vous pouvez ranger `ansible`, `terraform` et le dernier outil DevOps à la mode. Nous allons faire les choses plus simplement !


## Compilation

Rust est un langage compilé. Il faut donc créer un premier _job_ pour compiler le projet. La compilation s'effectue à l'aide de `cargo` :

```
cargo build --release
```

Cette commande va créer l'exécutable `answer`, dans le répertoire `target/release/`. Cet exécutable est auto-suffisant : il sert directement les routes de l'application. Il n'y a pas besoin d'un serveur `apache` ou `nginx`. Il suffit de le lancer avec la commande `./answer` et le serveur web est en route !

Bien sûr, pour que la compilation puisse se faire, il faut que `cargo` soit installé. Le plus simple est d'utiliser une image avec tout ce qu'il faut. Il est recommandé d'utiliser l'image officielle `rust:1.64`.

N'oubliez pas de déclarer le résultat de la compilation comme un artéfact. Vous pourrez le télécharger et le tester en local, mais aussi vous en servir dans les _jobs_ suivants.


## Déploiement

Nous allons déployer l'application sur un serveur privé et temporaire : [http://lab.timotheegerber.fr](http://lab.timotheegerber.fr). Vous avez déjà un compte utilisateur sur le serveur et vous pouvez vous connecter via SSH avec comme identifiant votre prénom en miniscules et comme mot de passe le préfix `campus_` + votre prénom en minuscules. Comme ce n'est pas très sécurisé, nous allons faire deux maipulations :

 1. changer de mot de passe grâce à la commande `passwd` ;
 2. générer une paire de clés (via `ssh-keygen`) et enregistrer la clé publique grâce à la commande `ssh-copy-id` (ou en manipulant le fichier `.ssh/authorized_keys` du serveur directement).

Pour déployer l'application sur le serveur, nous allons nous servir des commandes `scp` pour copier l'exécutable sur le serveur et `ssh` pour lancer des commandes sur le serveur. N'oubliez pas de sécuriser le _job_ de déploiement en enregistrant votre clé privée comme variable secrète !

Voici quelques petites choses qui pourront vous servir : `StrictHostKeyChecking=no` et `nohup`.


Bon courage et bon déploiement à tous !
