use axum::{routing::get, Json, Router};
use serde_json::{json, Value};
use std::env;

#[tokio::main]
async fn main() {
    // Register all the routes
    let app = Router::new().route("/", get(hello)).route(
        "/answer_to_life_the_universe_and_everything/",
        get(answer_to_life),
    );

    let app_port = env::var("APP_PORT").unwrap_or("3000".into());

    // Start the web server (listening on all network interfaces)
    axum::Server::bind(&format!("0.0.0.0:{app_port}").parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

/// Il n'a pas dit boujour.
/// Du coup, il s'est fait n***er sa mère !
async fn hello() -> &'static str {
    "Salut le monde !"
}

/// The answer to life, the universe and everything is 42.
/// Source: The Hitch Hiker's Guide to the Galaxy (1979) from Douglas Adams
async fn answer_to_life() -> Json<Value> {
    Json(json!({ "anwser": 42 }))
}
